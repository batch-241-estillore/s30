db.fruits.aggregate([
		{$match: {onSale: true}},
		{$count: "fruitsOnSale"}
	]);

db.fruits.aggregate([
		{$match: {stock: {$gt: 20} }},
		{$count: "fruitsStockGt20"}
	]);

db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", average: {$avg: "$price"}}}
	]);

db.fruits.aggregate([
		{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}}
	]);

db.fruits.aggregate([
		{$group: {_id: "$supplier_id", min_price: {$min: "$price"}}}
	]);
